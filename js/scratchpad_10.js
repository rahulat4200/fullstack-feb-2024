// function add(a, b, ...nums) {
// 	console.log(a);
// 	console.log(b);
// 	console.log(nums);
// }

// add(10, 20, 30, 40, 50);

// function add(...nums) {
// 	let total = 0;
// 	for (let num of nums) {
// 		total += num;
// 	}
// 	return total;
// }

// console.log(add(10, 20, 30, 40, 50));

// const users = ['john', 'jane', 'jack'];

// // const admin = users[0];
// // const mod = users[1];
// // const sub = users[2];

// // const [admin, ...others] = users;
// const [admin, , mod] = users;

// console.log(admin, mod);

// const user = {
// 	firstName: 'John',
// 	lastName: 'Doe',
// 	email: 'john.doe@gmail.com',
// 	phone: 99982234567,
// };

// const { firstName, lastName, email: emailAddress, phone } = user;

// // const firstName = user.firstName;
// // const lastName = user.lastName;
// // const email = user.email;
// // const phone = user.phone;

// console.log(firstName, lastName, emailAddress, phone);

// function profile(hello, { firstName, lastName, age }) {
// 	console.log(hello);
// 	console.log(
// 		`Hello, my name is ${firstName} ${lastName} and I am ${age} years old.`
// 	);
// }

// profile(10, { firstName: 'Jane', lastName: 'Doe', age: 20 });
