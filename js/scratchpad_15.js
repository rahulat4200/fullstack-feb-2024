// // fetch('https://pokeapi.co/api/v2/poksdakjfnlsdfnlsjdkfemon')
// // 	.then((response) => {
// // 		if (!response.ok) {
// // 			throw new Error('Something went wrong');
// // 		}
// // 		return response.json();
// // 	})
// // 	.then((data) => {
// // 		console.log(data);
// // 	})
// // 	.catch((err) => {
// // 		console.log('In the catch block');
// // 		console.log(err.message);
// // 	});

// // axios
// // 	.get('https://pokeapi.co/api/v2/pokemon')
// // 	.then((response) => {
// // 		console.log('In the then block');
// // 		console.log(response.data);
// // 	})
// // 	.catch((err) => {
// // 		console.log('In the catch block');
// // 		console.log(err.message);
// // 	});

// const root = document.querySelector('#root');

// const pokemonList = document.createElement('section');
// pokemonList.style.display = 'grid';
// pokemonList.style.gridTemplateColumns = '1fr 1fr 1fr 1fr 1fr';
// pokemonList.style.gap = '30px';

// axios
// 	.get('https://pokeapi.co/api/v2/pokemon?limit=100000')
// 	.then((response) => {
// 		for (let pokemon of response.data.results) {
// 			axios.get(pokemon.url).then((res) => {
// 				const img = document.createElement('img');
// 				img.src = res.data.sprites.other['official-artwork'].front_default;
// 				img.style.width = '100%';

// 				const name = document.createElement('h4');
// 				name.innerText = res.data.name;
// 				name.style.textAlign = 'center';
// 				name.style.marginTop = '12px';

// 				const card = document.createElement('article');
// 				card.append(img, name);

// 				pokemonList.append(card);
// 			});

// 			// const p = document.createElement('p');
// 			// p.innerText = pokemon.name;
// 			// pokemonList.append(p);
// 		}
// 	})
// 	.catch((err) => {
// 		console.log(err.message);
// 	});

// root.append(pokemonList);

// async function greet() {
// 	throw 'Hello World';
// }

// // function greet() {
// // 	return new Promise((resolve, reject) => {
// // 		reject('Hello World');
// // 	});
// // }

// console.log(greet());

// async function add(x, y) {
// 	if (typeof x !== 'number' || typeof y !== 'number') {
// 		throw 'X and Y should be numbers';
// 	}

// 	return x + y;
// }

// add(10, '14')
// 	.then((result) => console.log(result))
// 	.catch((err) => console.log(err));

async function getPokemon() {
	try {
		const response = await axios.get('https://pokeapi.co/api/v2/pokemon');
		console.log(response);
	} catch (err) {
		console.log(err.message);
	}
}

getPokemon();
