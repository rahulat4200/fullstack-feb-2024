// // // function multiply(x, y) {
// // // 	return x * y;
// // // }

// // // function square(x) {
// // // 	return multiply(x, x);
// // // }

// // // function rightTriangle(x, y, z) {
// // // 	return square(x) + square(y) === square(z);
// // // }

// // // console.log(rightTriangle(10, 20, 30));

// // // console.log('The first log');
// // // alert('Hello World');
// // // console.log('The second log');

// // // console.log('The first log');
// // // setTimeout(function () {
// // // 	console.log('Hello World');
// // // }, 5000);
// // // console.log('The second log');

// // const btn = document.querySelector('button');

// // setTimeout(function () {
// // 	btn.style.transform = `translateX(100px)`;
// // 	setTimeout(function () {
// // 		btn.style.transform = `translateX(200px)`;
// // 		setTimeout(function () {
// // 			btn.style.transform = `translateX(300px)`;
// // 			setTimeout(function () {
// // 				btn.style.transform = `translateX(400px)`;
// // 				setTimeout(function () {
// // 					btn.style.transform = `translateX(500px)`;
// // 					setTimeout(function () {
// // 						btn.style.transform = `translateX(600px)`;
// // 					}, 1000);
// // 				}, 1000);
// // 			}, 1000);
// // 		}, 1000);
// // 	}, 1000);
// // }, 1000);

// // const willGetAPlaystation = new Promise((resolve, reject) => {
// // 	const num = Math.random();

// // 	if (num > 0.5) {
// // 		resolve();
// // 	} else {
// // 		reject();
// // 	}
// // });

// // willGetAPlaystation
// // 	.then(() => {
// // 		console.log('Thank you uncle');
// // 	})
// // 	.catch(() => {
// // 		console.log('F you uncle');
// // 	});

// function makePlaystationPromise() {
// 	return new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 			const rand = Math.random();
// 			if (rand < 0.5) {
// 				resolve();
// 			} else {
// 				reject();
// 			}
// 		}, 3000);
// 	});
// }

// const result = makePlaystationPromise();

// result
// 	.then(() => {
// 		console.log('Thank you');
// 	})
// 	.catch(() => {
// 		console.log('##$#@$#@$');
// 	});

// console.log('Hello World');

function fakeRequest(url) {
	return new Promise((resolve, reject) => {
		setTimeout(function () {
			const pages = {
				'/users': [
					{ id: 1, username: 'john' },
					{ id: 2, username: 'jane' },
				],
				'/about': 'This is the about page',
				'/users/1': {
					id: 1,
					username: 'johndoe',
					topPostId: 53231,
					city: 'mumbai',
				},
				'/users/5': {
					id: 1,
					username: 'janedoe',
					topPostId: 32443,
					city: 'pune',
				},
				'/posts/53231': {
					id: 1,
					title: 'Really amazing post',
					slug: 'really-amazing-post',
				},
			};

			const data = pages[url];

			if (data) {
				resolve({ status: 200, data });
			} else {
				reject({ status: 404 });
			}
		}, 2000);
	});
}

fakeRequest('/users')
	.then((res) => {
		console.log('The first promise was resolved');
		const userId = res.data[0].id;
		return fakeRequest(`/users/${userId}`);
	})
	.then((res) => {
		console.log('The second promise was resolved');
		const topPostId = res.data.topPostId;
		return fakeRequest(`/posts/${topPostId}`);
	})
	.then((res) => {
		console.log('The third promise was resolved');
		console.log(res);
	})
	.catch((err) => {
		console.log(err);
	});
