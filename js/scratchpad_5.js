// const product = [
// 	1,
// 	'Apple',
// 	'iPhone 15',
// 	'Some description...',
// 	'https://img_url...',
// 	500,
// 	100000,
// 	true,
// ];

// console.log(product[2]);
// console.log(product[4]);

const product = {
	brand: 'Apple',
	name: 'iPhone 15',
	description: 'Some description...',
	imageUrl: 'https://img_url...',
	srNo: 1,
	price: 100000,
	inStock: 500,
	discounted: true,
	10: true,
	'hello world': false,
};

product.discounted;
product['10'];
product.name;
product['hello world'];

// 1,
// 'Apple',
// 'iPhone 15',
// 'Some description...',
// 'https://img_url...',
// 500,
// 100000,
// true,
