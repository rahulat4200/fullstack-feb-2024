longestWord('hello world this is something');
// "something"

isPalindrome('kayak');
// true

capitalizeStr('hello world this is a test message');
// Hello World This Is A Test Message

max(10, 20, 30, 100, 8, 3);
// 100
