// for (let count = 0; count < 10; count++) {
// 	console.log(count, 'Hello World');
// }

// for (let count = 10; count >= 0; count--) {
// 	console.log(count, 'Hello World');
// }

// const nums = [12, 34, 56, 34, 78, 54, 23, 12, 100];

// let total = 0;
// for (let i = 0; i < nums.length; i++) {
// 	// console.log(nums[i]);
// 	total += nums[i];
// }

// console.log(total);

// const movies = [
// 	{ movieName: 'Inception', rating: 3.8 },
// 	{ movieName: 'Avengers', rating: 3.4 },
// 	{ movieName: 'Iron Man', rating: 2.9 },
// ];

// for (let i = 0; i < movies.length; i++) {
// 	const movie = movies[i];
// 	console.log(`${movie.movieName} has a rating of ${movie.rating}`);
// }

// const message = 'Hello World';

// let reversedMessage = '';

// for (let i = message.length - 1; i >= 0; i--) {
// 	// console.log(message[i]);
// 	reversedMessage += message[i];
// }

// for (let j = 0; j < 5; j++) {
// 	console.log(`    ${j} - INNER LOOP`);
// }
// console.log(reversedMessage);

// for (let i = 0; i < 5; i++) {
// 	console.log(`${i} - OUTER LOOP`);

// 	for (let j = 0; j < 5; j++) {
// 		console.log(`    ${j} - INNER LOOP`);
// 	}
// }

// i = 1  // j = 5
// 0 - OUTER LOOP
//     0 - INNER LOOP
//     1 - INNER LOOP
//     2 - INNER LOOP
//     3 - INNER LOOP
//     4 - INNER LOOP
// 1 - OUTER LOOP
//     0 - INNER LOOP
//     1 - INNER LOOP
//     2 - INNER LOOP
//     3 - INNER LOOP
//     4 - INNER LOOP

// const gameBoard = [
// 	[4, 64, 8, 4],
// 	[128, 32, 4, 16],
// 	[16, 4, 4, 32],
// 	[2, 16, 16, 2],
// ];

// for (let i = 0; i < gameBoard.length; i++) {
// 	// console.log(gameBoard[i]);
// 	for (let j = 0; j < gameBoard[i].length; j++) {
// 		console.log(gameBoard[i][j]);
// 	}
// }

// for (let i = 0; i < 5; i++) {
// 	console.log(i);
// }

// let i = 0;

// while (i < 5) {
// 	console.log(i);
// 	i++;
// }

// let randomNum = Math.floor(Math.random() * 10) + 1;
// let guess = Math.floor(Math.random() * 10) + 1;

// while (true) {
// 	console.log(`RANDOM NUM: ${randomNum} | GUESS: ${guess}`);
// 	if (randomNum === guess) {
// 		break;
// 	}
// 	guess = Math.floor(Math.random() * 10) + 1;
// }

// console.log(`Final Result\nRANDOM NUM: ${randomNum} | GUESS: ${guess}`);

// let nums = [1, 2, 234, 435, 4, 22, 1];

// // for (let i = 0; i < nums.length; i++) {
// // 	console.log(nums[i]);
// // }

// for (let num of nums) {
// 	console.log(num);
// }

// for (let char of 'hello world') {
// 	console.log(char);
// }

// const matrix = [
// 	[1, 4, 7],
// 	[9, 7, 2],
// 	[9, 4, 6],
// ];

// for (let row of matrix) {
// 	for (let num of row) {
// 		console.log(num);
// 	}
// }

const cats = ['fashion', 'mobiles', 'books'];
const prods = ['tshirt', 'samsung', '1984'];

for (let i = 0; i < cats.length; i++) {
	console.log(cats[i], prods[i]);
}
