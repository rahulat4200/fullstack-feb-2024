// const nums = [1, 2, 3, 4];

// for (let num of nums) {
// 	console.log(num);
// }

// const productPrices = {
// 	Apple: 80000,
// 	OnePlus: 50000,
// 	Samsung: 90000,
// };

// for (let key of Object.keys(productPrices)) {
// 	console.log(key, productPrices[key]);
// }

// for (let key in productPrices) {
// 	console.log(key, productPrices[key]);
// }

// const movieRating = {
// 	pursuitOfHappiness: 4.8,
// 	satya: 4.8,
// 	gangsOfWasepur: 4,
// 	robot: -3,
// };

// for (let movie in movieRating) {
// 	console.log(movie, movieRating[movie]);
// }

// DRY

// function greet() {
// 	console.log('Hello World');
// 	console.log('This is some test text');
// }

// greet();
// greet();
// greet();
// greet();

// function flipCoin() {
// 	const random = Math.random();
// 	if (random > 0.5) {
// 		console.log('HEADS');
// 	} else {
// 		console.log('TAILS');
// 	}
// }

// function flipCoinTwice() {
// 	flipCoin();
// 	flipCoin();
// }

// flipCoinTwice();

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice() {
// 	rollDie();
// 	rollDie();
// 	rollDie();
// 	rollDie();
// }

// throwDice();

// function greet(name, message) {
// 	console.log(`${message}, ${name}`);
// }

// greet('Jane');
// greet('John', 'Hi');
// greet('Welcome', 'Jack');

// function greet(name) {
// 	console.log(`Hello ${name}, how are you?`);
// }

// greet('jack');

// function add(a, b) {
// 	console.log(a + b);
// }

// add(10, 20);

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice(times) {
// 	for (let i = 0; i < times; i++) {
// 		rollDie();
// 	}
// }

// throwDice(10);

// let fullName = 'Jane Doe';

// function greet() {
// 	console.log(`Hello ${fullName}`);
// }

// greet();

// let result = 'HELLO';

// function greet() {
// 	console.log('This is something');
// 	return 'Hello World';
// }

// console.log(greet());

// const result = greet();

// // let result = greet();

// console.log(`The value of result is: ${result}`);

// function isNumber(num) {
// 	if (typeof num !== 'number') {
// 		return false;
// 	}

// 	return true;
// }

// console.log(isNumber(10));
