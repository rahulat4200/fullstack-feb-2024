// // const movieReviews = [4.5, 5.0, 3.2, 2.1, 4.7, 3.8, 3.1, 3.9, 4.4];
// // const highest = Math.max(...movieReviews);
// // const lowest = Math.min(...movieReviews);

// // let total = 0;
// // movieReviews.forEach((rating) => (total += rating));
// // const average = total / movieReviews.length;

// // const info = { highest, lowest, average, hello: 'world' };

// // console.log(info);

// // const username = 'janedoe';
// // const role = 'admin';

// // const user = { [role]: username };

// // console.log(user);

// // const addProp = (obj, k, v) => {
// // 	return { ...obj, [k]: v };
// // };

// // const user = { firstName: 'John' };

// // console.log(addProp(user, 'lastName', 'Doe'));

// // const math = {
// // 	add(a, b) {
// // 		return a + b;
// // 	},
// // 	sub(a, b) {
// // 		return a - b;
// // 	},
// // 	mul(a, b) {
// // 		return a * b;
// // 	},
// // 	div(a, b) {
// // 		return a / b;
// // 	},
// // };

// // console.log(math.sub(10, 20));

// // console.log('Hello World');

// // function greet() {
// // 	console.log(this);
// // }

// // console.log(window);

// // window.greet();
// // greet();

// // const user = {
// // 	firstName: 'Jane',
// // 	lastName: 'Doe',
// // 	age: 20,
// // 	greet() {
// // 		console.log(this);
// // 	},
// // };

// // user.greet();

// // let aaaaaaaa1 = 'hello world';

// // console.log(window);

// // function greet() {
// // 	console.log(
// // 		`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// // 	);
// // }

// // const user1 = {
// // 	firstName: 'Jane',
// // 	lastName: 'Doe',
// // 	age: 20,
// // 	greet,
// // };

// const user2 = {
// 	firstName: 'John',
// 	lastName: 'Roe',
// 	age: 23,
// 	greet() {
// 		console.log(
// 			`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// 		);
// 	},
// };

// const hello = user2.greet;

// user2.greet();
// // hello();\

const hellos = {
	messages: [
		'hello world',
		'hello universe',
		'hello darkness',
		'hello hello',
		'heylo',
	],
	pickMsg: function () {
		const index = Math.floor(Math.random() * this.messages.length);
		return this.messages[index];
	},
	start: function () {
		setInterval(() => {
			console.log(this.pickMsg());
		}, 1000);
	},
};

// console.log(hellos.pickMsg());
// hellos.start();

// const user = {
// 	firstName: 'John',
// 	lastName: 'Roe',
// 	age: 23,
// 	greet: () => {
// 		// this = window
// 		console.log(this);
// 	},
// };

// user.greet();
