// // function initializeDeck() {
// // 	const deck = [];
// // 	const suits = ['hearts', 'diamonds', 'spades', 'clubs'];
// // 	const values = '2,3,4,5,6,7,8,9,10,J,Q,K,A';
// // 	for (let value of values.split(',')) {
// // 		for (let suit of suits) {
// // 			deck.push({ value, suit });
// // 		}
// // 	}
// // 	return deck;
// // }

// // function drawCard(deck, drawnCards) {
// // 	const card = deck.pop();
// // 	drawnCards.push(card);
// // 	return card;
// // }

// // function drawMultiple(numCards, deck, drawnCards) {
// // 	const cards = [];
// // 	for (let i = 0; i < numCards; i++) {
// // 		cards.push(drawCard(deck, drawnCards));
// // 	}
// // 	return cards;
// // }

// // function shuffle(deck) {
// // 	// loop over array backwards
// // 	for (let i = deck.length - 1; i > 0; i--) {
// // 		// pick random index before current element
// // 		let j = Math.floor(Math.random() * (i + 1));
// // 		[deck[i], deck[j]] = [deck[j], deck[i]];
// // 	}
// // 	return deck;
// // }

// // const deck1 = initializeDeck();
// // const hand = [];

// // shuffle(deck1);
// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawMultiple(5, deck1, hand);
// // console.log(deck1);
// // console.log(hand);

// function makeDeck() {
// 	return {
// 		deck: [],
// 		drawnCards: [],
// 		suits: ['hearts', 'diamonds', 'spades', 'clubs'],
// 		values: '2,3,4,5,6,7,8,9,10,J,Q,K,A',
// 		initializeDeck() {
// 			for (let value of this.values.split(',')) {
// 				for (let suit of this.suits) {
// 					this.deck.push({ value, suit });
// 				}
// 			}
// 			return this.deck;
// 		},
// 		drawCard() {
// 			const card = this.deck.pop();
// 			this.drawnCards.push(card);
// 			return card;
// 		},
// 		drawMultiple(numCards) {
// 			const cards = [];
// 			for (let i = 0; i < numCards; i++) {
// 				cards.push(this.drawCard());
// 			}
// 			return cards;
// 		},
// 		shuffle() {
// 			const { deck } = this;
// 			for (let i = deck.length - 1; i > 0; i--) {
// 				let j = Math.floor(Math.random() * (i + 1));
// 				[deck[i], deck[j]] = [deck[j], deck[i]];
// 			}
// 		},
// 	};
// }

// const deck1 = makeDeck();
// const deck2 = makeDeck();

// console.log(deck1);
// console.log(deck2);

// // deck1.initializeDeck();
// // deck1.shuffle();
// // deck1.drawMultiple(10);

// // console.log(deck1);

// function user(firstName, lastName, age) {
// 	return {
// 		firstName,
// 		lastName,
// 		age,
// 	};
// }

// function User(firstName, lastName, age) {
// 	this.firstName = firstName;
// 	this.lastName = lastName;
// 	this.age = age;
// }
// User.prototype.greet = function () {
// 	console.log(`Hello, my name is ${this.firstName} ${this.lastName}`);
// };

// class User {
// 	constructor(firstName, lastName, age) {
// 		this.firstName = firstName;
// 		this.lastName = lastName;
// 		this.age = age;
// 	}

// 	greet() {
// 		console.log(`Hello, my name is ${this.firstName} ${this.lastName}`);
// 	}
// }

// const jane = { firstName: 'Jane', lastName: 'Smith', age: 30 };
// const jack = new User('Jack', 'Roe', 40);

// console.log(jane);
// console.log(jack);
// jack.greet();

class User {
	constructor(firstName, lastName, age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	login() {
		console.log('User just logged in');
	}

	logout() {
		console.log('User just logged out');
	}
}

class Admin extends User {
	constructor(firstName, lastName, age, phone) {
		super(firstName, lastName, age);
		this.phone = phone;
	}

	createGroup(groupName) {
		console.log(`${groupName} group was created by ${this.firstName}`);
	}
}

const user1 = new User('John', 'Doe', 20);
const user2 = new Admin('Jane', 'Smith', 30);

user1.logout();
user2.logout();
user2.createGroup('JavaScript');
