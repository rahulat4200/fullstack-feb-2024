// let age = 30;

// if (age >= 18 && age < 21) {
// 	console.log('You can enter, but you cannot drink');
// } else if (age >= 21 && age < 65) {
// 	console.log('You can enter and you can drink');
// } else if (age >= 65) {
// 	console.log('Drinks are free');
// } else {
// 	console.log("You aren't allowed");
// }

// let day = 'hello';

// switch (day) {
// 	case 'hello':
// 		console.log('Mon');
// 		break;
// 	case 2:
// 		console.log('Tue');
// 		break;
// 	case 3:
// 		console.log('Wed');
// 		break;
// 	case 4:
// 		console.log('Thu');
// 		break;
// 	case 5:
// 		console.log('Fri');
// 		break;
// 	case 6:
// 		console.log('Sat');
// 		break;
// 	case 7:
// 		console.log('Sun');
// 		break;
// 	default:
// 		console.log('Invalid day code');
// }

// if (day === 1) {
// 	console.log('Mon');
// } else if (day === 2) {
// 	console.log('Tue');
// } else if (day === 3) {
// 	console.log('Wed');
// } else if (day === 4) {
// 	console.log('Thu');
// } else if (day === 5) {
// 	console.log('Fri');
// } else if (day === 6) {
// 	console.log('Sat');
// } else if (day === 7) {
// 	console.log('Sun');
// } else {
// 	console.log('Invalid day code');
// }

let statusCode = 'offline';

let color =
	statusCode === 'online'
		? 'green'
		: statusCode === 'offline'
		? 'red'
		: statusCode === 'idle'
		? 'yellow'
		: 'gray';

// let color;

// if (statusCode === 'online') {
// 	color = 'green';
// } else if (statusCode === 'offline') {
// 	color = 'red';
// } else {
// 	color = 'yellow';
// }

console.log(color);
