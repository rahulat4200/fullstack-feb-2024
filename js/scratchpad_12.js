// // // console.dir(document.all);

// // // const el = document.getElementById('title');
// // // console.dir(el);

// // // const lis = document.getElementsByTagName('li');
// // // console.log(lis);

// // // const lists = document.getElementsByClassName('imp-list');
// // // console.log(lists);

// // // const ul = document.getElementsByTagName('ul')[1];

// // // const lis = ul.getElementsByTagName('li');
// // // console.log(lis);

// // // const lis = document.querySelector('#title');
// // // console.log(lis);

// // // const h1 = document.querySelector('h1');
// // // // console.log(h1.innerText);
// // // h1.innerText = new Date().toLocaleDateString();

// // // const ul = document.querySelector('ul');
// // // // console.log(ul);
// // // ul.innerText = 'hello world';

// // // const p = document.querySelector('.special');
// // // console.log(p.textContent);

// // // const h1 = document.querySelector('h1');
// // // // console.log(h1.innerText);

// // // h1.innerHTML = 'This is <u>something</u>';
// // // h1.innerHTML += '-----';

// // // const link = document.querySelector('a');
// // // console.log(link.href);

// // // link.href = 'https://duckduckgo.com';
// // // link.id = 'test';
// // // link.className = 'special-red-link';

// // // console.log(link.href);
// // // console.log(link.getAttribute('href'));

// // // console.log(link.id);
// // // console.log(link.getAttribute('id'));

// // // link.id = 'hello';
// // // link.setAttribute('id', 'world');

// // // const li = document.querySelector('#unique');
// // // // console.log(li.parentElement.parentElement.parentElement);
// // // console.log(li.children);

// // // const lis = document.querySelectorAll('li');

// // // for (let li of lis) {
// // // 	setInterval(() => {
// // // 		li.innerText = Math.random();
// // // 	}, 100);
// // // }

// // // const h1 = document.querySelector('h1');

// // // h1.style.color = 'red';
// // // h1.style.backgroundColor = 'yellow';

// // // document.body.style.backgroundColor = 'orange';

const colors = [
	'red',
	'green',
	'blue',
	'purple',
	'orange',
	'brown',
	'yellow',
	'pink',
	'violet',
	'gold',
	'aqua',
];

// // // const lis = document.querySelectorAll('li');

// // // setInterval(function () {
// // // 	const num = Math.floor(Math.random() * colors.length);
// // // 	document.body.style.backgroundColor = colors[num];
// // // }, 1000);

// // // for (let li of lis) {
// // // 	setInterval(function () {
// // // 		const num = Math.floor(Math.random() * colors.length);
// // // 		li.style.color = colors[num];
// // // 	}, 1000);
// // // }

// // const li = document.querySelectorAll('li')[1];

// // // console.log(li.innerText);
// // // li.className = 'todo done';

// // // console.log(li.classList);
// // li.classList.add('done');
// // li.classList.add('hello');
// // li.classList.remove('done');
// // li.classList.toggle('todo');

// // const root = document.querySelector('#root');

// // const title = document.createElement('h2');
// // title.innerText = 'Hello World';
// // title.style.color = 'red';
// // // <h1 style="color: red;">Hello World</h1>

// // root.appendChild(title);

// // const root = document.querySelector('#root');

// // const div = document.createElement('section');

// // const h2 = document.createElement('h2');
// // h2.innerText = 'This is some text';

// // const p = document.createElement('p');
// // p.innerText =
// // 	'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis consectetur.';

// // div.appendChild(h2);
// // div.appendChild(p);

// // root.appendChild(div);

// // const ul = document.querySelector('ul');

// // const li = document.createElement('li');
// // li.innerText = 'This is a new li element';
// // li.style.color = 'green';

// // const secondLi = document.querySelectorAll('li')[1];

// // // ul.appendChild(li);

// // ul.insertBefore(li, secondLi);

// const p = document.querySelector('p');

// const b = document.createElement('b');
// b.innerText = 'This is bold';

// const i = document.createElement('i');
// i.innerText = 'This is some italic text';

// p.prepend(b);

// p.remove();

// const btn = document.querySelector('button');
// console.log(btn);

// const p = document.querySelector('p');

// p.onmouseleave = function () {
// 	const num = Math.floor(Math.random() * colors.length);
// 	document.body.style.backgroundColor = colors[num];
// };

// const btn = document.querySelector('button');

// btn.addEventListener('click', function () {
// 	const num = Math.floor(Math.random() * colors.length);
// 	document.body.style.backgroundColor = colors[num];
// });

// btn.addEventListener('click', function () {
// 	console.log('Hello World');
// });

// btn.onclick = function () {
// 	const num = Math.floor(Math.random() * colors.length);
// 	document.body.style.backgroundColor = colors[num];
// };

// btn.onclick = function () {
// 	console.log('Hello World');
// };

// window.addEventListener('scroll', function () {
// 	console.log('Scrolled');
// });

// const btn = document.querySelector('button');
// btn.style.position = 'relative';

// btn.addEventListener('mouseover', function () {
// 	const height = Math.floor(Math.random() * window.innerHeight);
// 	const width = Math.floor(Math.random() * window.innerWidth);
// 	btn.style.left = `${width}px`;
// 	btn.style.top = `${height}px`;
// });

// btn.addEventListener('click', function () {
// 	btn.innerText = 'You win!';
// 	document.body.style.backgroundColor = 'green';
// });

const root = document.querySelector('#root');

const title = document.createElement('h3');
title.innerText = 'TODO Manager';

const input = document.createElement('input');
input.placeholder = 'Enter a task';

const addBtn = document.createElement('button');
addBtn.innerText = 'Add';

const section = document.createElement('section');
section.append(title, input, addBtn);

const listOfTodos = document.createElement('section');
const listTitle = document.createElement('h3');
listTitle.innerText = 'My Todos';
const ul = document.createElement('ul');
listOfTodos.append(listTitle, ul);

addBtn.addEventListener('click', function () {
	const todo = document.createElement('li');
	todo.innerText = input.value;
	todo.addEventListener('click', function () {
		todo.classList.toggle('done');
	});
	const delBtn = document.createElement('button');
	delBtn.innerText = 'Delete';
	delBtn.addEventListener('click', function () {
		todo.remove();
	});
	todo.append(delBtn);
	ul.append(todo);
	input.value = '';
});

root.append(section, listOfTodos);
